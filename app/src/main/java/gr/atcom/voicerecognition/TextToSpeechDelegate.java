package gr.atcom.voicerecognition;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.Voice;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TextToSpeechDelegate extends UtteranceProgressListener implements TextToSpeech.OnInitListener {

    private final String TAG = TextToSpeechDelegate.class.getSimpleName();

    @Nullable
    private TextToSpeech textToSpeech;

    @NonNull
    private final ITextToSpeechDelegate listener;

    public TextToSpeechDelegate(@NonNull ITextToSpeechDelegate listener) {
        this.listener = listener;
    }

    public void init(@NonNull Context context) {
        textToSpeech = new TextToSpeech(context, this);
        textToSpeech.setOnUtteranceProgressListener(this);
    }

    public void speak(String text) {
        if(textToSpeech != null) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String myUtteranceID = "myUtteranceID";
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, myUtteranceID);
            }
            else {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "myUtteranceID");
                textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, hashMap);
            }
        }
    }

    public void setVoice(@NonNull Voice voice) {
        if (textToSpeech == null) {
            return;
        }

        textToSpeech.setVoice(voice);
    }

    @NonNull
    private List<Voice> getVoices() {
        if (textToSpeech == null) {
            return new ArrayList<>();
        }

        List<Voice> englishVoices = new ArrayList<>();
        for (Voice voice: textToSpeech.getVoices()) {
            final Locale locale = voice.getLocale();

            if (locale == Locale.ENGLISH || locale == Locale.UK || locale == Locale.CANADA ||
            locale == Locale.US) {
                englishVoices.add(voice);
            }
        }

        return englishVoices;
    }

    public void stop() {
        if (textToSpeech == null) {
            return;
        }

        textToSpeech.stop();
    }

    public void shutdown() {
        if (textToSpeech == null) {
            return;
        }

        textToSpeech.shutdown();
    }

    public boolean isSpeaking() {
        if (textToSpeech == null) {
            return false;
        }

        return textToSpeech.isSpeaking();
    }

    @Override
    public void onInit(int status) {
        if (textToSpeech == null) {
            return;
        }

        if(status != TextToSpeech.ERROR) {
            listener.onFetchedVoices(getVoices());
        }
    }

    @Override
    public void onStart(String utteranceId) {
//        Log.d(TAG, "onStart / utteranceID = " + utteranceId);
    }

    @Override
    public void onDone(String utteranceId) {
//        Log.d(TAG, "onDone / utteranceID = " + utteranceId);
    }

    @Override
    public void onError(String utteranceId) {
        Log.d(TAG, "onError / utteranceID = " + utteranceId);
    }

    @Override
    public void onError(String utteranceId, int errorCode) {
        Log.d(TAG, "onError / utteranceID = " + utteranceId);
        Log.d(TAG, "onError / errorCode = " + errorCode);
    }

    public interface ITextToSpeechDelegate {
        void onFetchedVoices(@NonNull List<Voice> voices);
    }
}
