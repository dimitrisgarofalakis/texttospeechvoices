package gr.atcom.voicerecognition;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements TextToSpeechDelegate.ITextToSpeechDelegate,
        VoicesRecyclerViewAdapter.IVoicesRecyclerViewAdapter {

    @NonNull
    private final TextToSpeechDelegate tts = new TextToSpeechDelegate(this);

    @NonNull
    private final VoicesRecyclerViewAdapter voicesRecyclerViewAdapter = new VoicesRecyclerViewAdapter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tts.init(this);

        final RecyclerView recyclerView = findViewById(R.id.voicesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(voicesRecyclerViewAdapter);
    }

    @Override
    protected void onDestroy() {
        if(tts.isSpeaking()) {
            tts.stop();
        }

        tts.shutdown();

        super.onDestroy();
    }


    @Override
    public void onFetchedVoices(@NonNull List<Voice> voices) {
        voicesRecyclerViewAdapter.setVoices(voices);
    }

    @Override
    public void onIVoicesRecyclerViewAdapterClick(@NonNull Voice voice) {
        tts.setVoice(voice);
        tts.speak("Hello World");
    }
}
