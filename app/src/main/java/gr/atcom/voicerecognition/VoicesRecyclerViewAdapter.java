package gr.atcom.voicerecognition;

import android.speech.tts.Voice;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class VoicesRecyclerViewAdapter extends RecyclerView.Adapter<VoicesRecyclerViewAdapter.ViewHolder> {

    @NonNull
    private List<Voice> voices;

    @NonNull
    private final IVoicesRecyclerViewAdapter listener;

    public VoicesRecyclerViewAdapter(@NonNull IVoicesRecyclerViewAdapter listener) {
        this.voices = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(
                LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_voice, viewGroup, false),
                this.listener
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Voice voice  = voices.get(i);
        viewHolder.bindVoice(voice.getName());
        viewHolder.bindClick(voice);
    }

    @Override
    public int getItemCount() {
        return voices.size();
    }

    public void setVoices(@NonNull List<Voice> voices) {
        this.voices = voices;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Nullable
        private final AppCompatTextView voiceDescription;

        @NonNull
        private final IVoicesRecyclerViewAdapter listener;

        public ViewHolder(@NonNull View itemView, @NonNull IVoicesRecyclerViewAdapter listener) {
            super(itemView);
            voiceDescription = itemView.findViewById(R.id.voiceDescription);
            this.listener = listener;
        }

        public void bindClick(final @NonNull Voice voice) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onIVoicesRecyclerViewAdapterClick(voice);
                }
            });
        }

        public void bindVoice(@Nullable CharSequence description) {
            if (voiceDescription == null) {
                return;
            }

            voiceDescription.setText(description);
        }
    }

    public interface IVoicesRecyclerViewAdapter {
        void onIVoicesRecyclerViewAdapterClick(@NonNull Voice voice);
    }

}
